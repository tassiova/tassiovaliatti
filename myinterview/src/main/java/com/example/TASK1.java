package com.example;
import java.util.Scanner;
/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
public class TASK1 {
     
     public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite uma palavra qualquer:");
        String word = sc.nextLine();
        sc.close();
        
        if(CheckPalindrome(word))
            System.out.print(word + " correto palíndromo");
        else 
            System.out.print(word + " incorreto palíndromo");
     }
     
    private static boolean CheckPalindrome (String original) {
        String reverse = "";
        int length = original.length();   
        for ( int i = length - 1; i >= 0; i-- )  
            reverse = reverse + original.charAt(i);  
        if (original.equals(reverse))  
            return true;
        else  
            return false;
    }
 
}